/*
 * SonarQube Integration for Confluence
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.confluence.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.DashboardPanelRenderer;

import java.util.Map;

/**
 * Macro to display a SonarQube Panel
 *
 * @author Mark Rekveld
 * @since 1.0.0-beta-2
 */
public class SonarQubePanelMacro implements Macro {

	private static final String SPACEKEY_PARAM = "space";
	private static final String DASHBOARD_PARAM = "dashboard";
	private DashboardPanelRenderer dashboardPanelRenderer;

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws
			MacroExecutionException {
		String spacekey = parameters.containsKey(SPACEKEY_PARAM) ? parameters.get(SPACEKEY_PARAM) : conversionContext.getSpaceKey();
		if (parameters.containsKey(DASHBOARD_PARAM)) {
			return dashboardPanelRenderer.getPanel(spacekey, Integer.parseInt(parameters.get(DASHBOARD_PARAM)));
		} else {
			return dashboardPanelRenderer.getPanel(spacekey);
		}
	}

	/**
	 * Setter for the {@link DashboardPanelRenderer}
	 *
	 * @param dashboardPanelRenderer the {@link DashboardPanelRenderer}
	 */
	public void setDashboardPanelRenderer(DashboardPanelRenderer dashboardPanelRenderer) {
		this.dashboardPanelRenderer = dashboardPanelRenderer;
	}

}
