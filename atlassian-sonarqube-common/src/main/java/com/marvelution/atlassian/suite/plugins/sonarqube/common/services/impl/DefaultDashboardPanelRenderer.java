/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services.impl;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.*;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;

/**
 * Default implementation of the {@link DashboardPanelRenderer}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultDashboardPanelRenderer implements DashboardPanelRenderer {

	private final HostApplicationFacade hostApplicationFacade;
	private final CommunicatorFactory communicatorFactory;
	private final TemplateRenderer templateRenderer;
	private final ApplicationProperties applicationProperties;
	private final WebResourceManager webResourceManager;

	/**
	 * Default Constructor
	 *
	 * @param hostApplicationFacade the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade}
	 * @param communicatorFactory   the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory}
	 * @param templateRenderer      the {@link com.atlassian.templaterenderer.TemplateRenderer}
	 * @param applicationProperties the {@link com.atlassian.sal.api.ApplicationProperties}
	 * @param webResourceManager    the {@link WebResourceManager}
	 */
	public DefaultDashboardPanelRenderer(HostApplicationFacade hostApplicationFacade, CommunicatorFactory communicatorFactory,
	                                     TemplateRenderer templateRenderer, ApplicationProperties applicationProperties,
	                                     WebResourceManager webResourceManager) {
		this.hostApplicationFacade = hostApplicationFacade;
		this.communicatorFactory = communicatorFactory;
		this.templateRenderer = templateRenderer;
		this.applicationProperties = applicationProperties;
		this.webResourceManager = webResourceManager;
	}

	@Override
	public String getPanel(String localKey) {
		return getPanel(localKey, -1);
	}

	@Override
	public String getPanel(String localKey, int dashboardId) {
		EntityLink primary = hostApplicationFacade.getEntityLink(localKey);
		if (primary != null) {
			Communicator communicator = communicatorFactory.get(primary.getApplicationLink());
			try {
				UriBuilder uriBuilder = UriBuilder.fromPath("/api/dashpanels");
				if (dashboardId == -1) {
					uriBuilder = uriBuilder.path("all");
				} else {
					uriBuilder = uriBuilder.queryParam("id", dashboardId);
				}
				RestResponse response = communicator.get(uriBuilder.queryParam("format", "json").queryParam("resource",
						primary.getKey()).build().toASCIIString());
				StringWriter writer = new StringWriter();
				HashMap<String, Object> context = Maps.newHashMap();
				context.put("webResourceManager", webResourceManager);
				context.put("baseUrl", applicationProperties.getBaseUrl());
				context.put("localKey", localKey);
				context.put("entityLink", primary);
				context.put("dashboardsJson", response.getResponseBody());
				templateRenderer.render("/templates/sonarqube-dashboard.vm", context, writer);
				return writer.toString();
			} catch (CredentialsRequiredException e) {
				return e.getMessage();
			} catch (IOException e) {
				return e.getMessage();
			}
		} else {
			return "No SonarQube Resource link found for " + localKey;
		}
	}

}
