/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services;

import com.atlassian.applinks.api.CredentialsRequiredException;

import java.util.Map;

/**
 * Communicator service interface
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface Communicator {

	/**
	 * Execute a GET request
	 *
	 * @param service the service url to execute
	 * @return the {@link RestResponse}
	 * @throws CredentialsRequiredException in case of auth related issues
	 */
	RestResponse get(String service) throws CredentialsRequiredException;

	/**
	 * Execute a GET request
	 *
	 * @param service the service url to execute
	 * @param headers map of extra request headers, may not be {@code null}
	 * @return the {@link RestResponse}
	 * @throws CredentialsRequiredException in case of auth related issues
	 */
	RestResponse get(String service, Map<String, String> headers) throws CredentialsRequiredException;

	/**
	 * Execute a GET request
	 *
	 * @param service the service url to execute
	 * @param timeout the timeout use
	 * @return the {@link RestResponse}
	 * @throws CredentialsRequiredException in case of auth related issues
	 */
	RestResponse get(String service, int timeout) throws CredentialsRequiredException;

	/**
	 * Execute a GET request
	 *
	 * @param service the service url to execute
	 * @param headers map of extra request headers, may not be {@code null}
	 * @param timeout the timeout use
	 * @return the {@link RestResponse}
	 * @throws CredentialsRequiredException in case of auth related issues
	 */
	RestResponse get(String service, Map<String, String> headers, int timeout) throws CredentialsRequiredException;

}
