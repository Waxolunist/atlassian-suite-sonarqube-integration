/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.applinks;

import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.utils.SonarQubeCommonPluginUtils;

/**
 * SonarQube Application Link Type
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class SonarQubeApplicationType extends IconizedIdentifiableType implements NonAppLinksApplicationType {

	private static final String SONAR = "sonarqube";
	public static final TypeId TYPE_ID = new TypeId(SONAR);

	/**
	 * Constructor
	 *
	 * @param pluginUtil             the {@link SonarQubeCommonPluginUtils}
	 * @param webResourceManager the {@link com.atlassian.plugin.webresource.WebResourceManager} implementation
	 */
	public SonarQubeApplicationType(SonarQubeCommonPluginUtils pluginUtil, WebResourceManager webResourceManager) {
		super(pluginUtil, webResourceManager);
	}

	@Override
	public String getI18nKey() {
		return SONAR;
	}

	@Override
	public TypeId getId() {
		return TYPE_ID;
	}

}
