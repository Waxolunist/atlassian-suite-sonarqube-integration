/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;

/**
 * Factory interface to provide {@link Communicator} implementations
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface CommunicatorFactory {

	/**
	 * Get a {@link Communicator} for the given {@link ApplicationId}
	 *
	 * @param applicationId the {@link ApplicationId}
	 * @return the {@link Communicator}
	 */
	Communicator get(ApplicationId applicationId);

	/**
	 * Get a {@link Communicator} for the given {@link ApplicationLink}
	 *
	 * @param applicationLink the {@link ApplicationLink}
	 * @return the {@link Communicator}
	 */
	Communicator get(ApplicationLink applicationLink);

}
