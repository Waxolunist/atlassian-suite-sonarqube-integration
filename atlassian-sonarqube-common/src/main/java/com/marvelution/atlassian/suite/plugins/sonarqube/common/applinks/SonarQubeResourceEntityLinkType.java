/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.spi.application.NonAppLinksEntityType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.util.Assertions;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.utils.SonarQubeCommonPluginUtils;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.utils.URIUtils;

import java.net.URI;

/**
 * SonarQube Entity Link Type
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class SonarQubeResourceEntityLinkType extends IconizedIdentifiableType implements NonAppLinksEntityType {

	public static final String SONAR_RESOURCE = "sonarqube.resource";
	private static final TypeId TYPE_ID = new TypeId(SONAR_RESOURCE);

	/**
	 * Constructor
	 *
	 * @param pluginUtil             the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.utils.SonarQubeCommonPluginUtils}
	 * @param webResourceManager the {@link com.atlassian.plugin.webresource.WebResourceManager} implementation
	 */
	public SonarQubeResourceEntityLinkType(SonarQubeCommonPluginUtils pluginUtil, WebResourceManager webResourceManager) {
		super(pluginUtil, webResourceManager);
	}

	@Override
	public TypeId getId() {
		return TYPE_ID;
	}

	@Override
	public Class<? extends ApplicationType> getApplicationType() {
		return SonarQubeApplicationType.class;
	}

	@Override
	public String getI18nKey() {
		return SONAR_RESOURCE;
	}

	@Override
	public String getPluralizedI18nKey() {
		return SONAR_RESOURCE + "s";
	}

	@Override
	public String getShortenedI18nKey() {
		return "resource";
	}

	@Override
	public URI getDisplayUrl(ApplicationLink applicationLink, String resource) {
		Assertions.isTrue(String.format("Application link %s is not of type %s", applicationLink.getId(),
				getApplicationType().getName()), (applicationLink.getType() instanceof SonarQubeApplicationType));
		return URIUtils.appendPathsToURI(applicationLink.getDisplayUrl(), "dashboard/index", resource);
	}

}
