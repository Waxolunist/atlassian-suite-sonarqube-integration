/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services.impl;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.applinks.api.application.bamboo.BambooProjectEntityType;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.util.Narrow;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReference;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReferences;
import org.acegisecurity.context.SecurityContextHolder;

import javax.annotation.Nullable;

/**
 * Bamboo Specific implementation for {@link AbstractHostApplicationFacade}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BambooApplicationFacade extends AbstractHostApplicationFacade {

	private final ProjectManager projectManager;
	private final PlanManager planManager;
	private final BambooPermissionManager permissionManager;
	private final Function<Project, EntityReference> projectToEntityReference = new Function<Project, EntityReference>() {
		@Override
		public EntityReference apply(@Nullable Project project) {
			if (project != null && canView(project)) {
				return new EntityReference(project.getKey(), project.getName());
			}
			return null;
		}
	};
	private final Predicate<Plan> canReadPlan = new Predicate<Plan>() {
		@Override
		public boolean apply(@Nullable Plan plan) {
			return plan != null && permissionManager.hasPermission(BambooPermission.READ, plan, null);
		}
	};

	/**
	 * Permission check for {@link Project}
	 *
	 * @param project the {@link Project} to check for
	 * @return {@code true} is the current user is a system administrator, or can read a plan of the project
	 */
	private boolean canView(Project project) {
		if (SecurityContextHolder.getContext().getAuthentication().equals(BambooPermissionManager.SYSTEM_AUTHORITY)) {
			return true;
		}
		Iterable<Plan> plans = Narrow.iterableTo(planManager.getPlansByProject(project), Plan.class);
		return Iterables.isEmpty(plans) || Iterables.any(plans, canReadPlan);
	}

	/**
	 * Constructor
	 *
	 * @param applicationLinkService the {@link com.atlassian.applinks.api.ApplicationLinkService}
	 * @param typeAccessor           the Applinks {@link com.atlassian.applinks.spi.util.TypeAccessor}
	 * @param entityLinkService      the {@link com.atlassian.applinks.api.EntityLinkService}
	 * @param projectManager         the {@link com.atlassian.bamboo.project.ProjectManager}
	 * @param planManager            the {@link PlanManager}
	 * @param permissionManager      the {@link com.atlassian.bamboo.security.BambooPermissionManager}
	 */
	public BambooApplicationFacade(ApplicationLinkService applicationLinkService, TypeAccessor typeAccessor,
	                               EntityLinkService entityLinkService, ProjectManager projectManager, PlanManager planManager,
	                               BambooPermissionManager permissionManager) {
		super(applicationLinkService, entityLinkService, typeAccessor, BambooApplicationType.class, BambooProjectEntityType.class);
		this.projectManager = projectManager;
		this.planManager = planManager;
		this.permissionManager = permissionManager;
	}

	@Override
	protected Object getRawEntity(String key) {
		return projectManager.getProjectByKey(key);
	}

	@Override
	public EntityReference getEntity(String key) {
		return projectToEntityReference.apply(projectManager.getProjectByKey(key));
	}

	@Override
	public EntityReferences getEntities() {
		return new EntityReferences(Iterables.transform(projectManager.getAllProjects(), projectToEntityReference));
	}

	@Override
	public boolean isAuthenticated() {
		return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
	}

	@Override
	public boolean isSystemAdministrator() {
		return SecurityContextHolder.getContext().getAuthentication().equals(BambooPermissionManager.SYSTEM_AUTHORITY);
	}

}
