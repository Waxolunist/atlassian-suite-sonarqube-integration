#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Check if the Applinks tables need to be created on startup
unless ActiveRecord::Base.connection.table_exists? 'application_links'
  ActiveRecord::Migration.class_eval do
    #drop_table :application_links
    #drop_table :entity_links

    create_table :application_links do |table|
      table.column :application_id,     :string,    :null => false,   :limit => 64
      table.column :type_id,            :string,    :null => false,   :limit => 64
      table.column :name,               :string,    :null => false,   :limit => 256
      table.column :display_url,        :string,    :null => false,   :limit => 512
      table.column :rpc_url,            :string,    :null => false,   :limit => 512
      table.column :icon_url,           :string,    :null => false,   :limit => 512
      table.column 'primare',           :boolean,   :null => true,    :default => false
      table.column :system,             :boolean,   :null => true,    :default => false
    end

    create_table :entity_links do |table|
      table.column :project_id,         :integer,   :null => false
      table.column :application_id,     :string,    :null => false,   :limit => 64
      table.column :type_id,            :string,    :null => false,   :limit => 64
      table.column 'kee',               :string,    :null => false,   :limit => 256
      table.column :name,               :string,    :null => false,   :limit => 256
      table.column 'primare',           :boolean,   :null => false,   :default => false
    end
  end
end
